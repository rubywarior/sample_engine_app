module MyCore
  module ApplicationHelper
    
    # Return the full title on a per-page basis
    def full_title(page_title = '')
      base_title = "Static Pages Engine App"
      page_title.empty? ? base_title : "#{page_title} | #{base_title}"
    end
    
  end
end
