$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "my_auth/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "my_auth"
  s.version     = MyAuth::VERSION
  s.authors     = ["eugen mueller"]
  s.email       = ["eugen_mll@arcor.de"]
  s.homepage    = "https://powerful-sands-5730.herokuapp.com/"
  s.summary     = "My Authentication App"
  s.description = "My Authentication App to Authenticate users"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.0"

  s.add_development_dependency "sqlite3"
  
  s.add_development_dependency 'minitest-reporters'
  
  s.add_development_dependency 'spring'
  
  s.add_development_dependency 'guard'
  
  s.add_development_dependency 'guard-minitest'
end
